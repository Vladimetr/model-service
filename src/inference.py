import os
from pydantic import BaseModel, FilePath
import argparse
import uvicorn
from fastapi import FastAPI, HTTPException
import torch
import torchaudio
import mlflow

class AudioPath(BaseModel):
    audio_path: FilePath


app = FastAPI()
mlflow.set_tracking_uri(os.getenv("MLFLOW_URL"))


class Model:
    def __init__(self, model_name: str, model_stage: str):
        """
        Initialize model
        model_name (str): name of model
        model_stage (str): stage of model
        """
        model_uri = f"models:/{model_name}/{model_stage}"
        self.model = mlflow.pytorch.load_model(model_uri)
        self.genders = ["male", "female"]

    def predict(self, sample: torch.Tensor) -> str:
        """
        Predict gender of speech in given audio
        Args:
            audio_path (str): existed/path/to/audio.wav
        Returns:
            str: "male" or "female"
        """
        probs = self.model.predict(sample)
        # Tensor (2, )
        cls_indx = torch.argmax(probs).item()
        gender = self.genders[cls_indx]
        return gender
    
# Create model
model = Model("cnn", "production")
    

@app.post('/predict')
async def predict(inp: AudioPath):
    inp = inp.dict()
    audio_path = inp[""]
    # Check path exists
    if not os.path.exists(audio_path):
        raise HTTPException(status_code=400,
                            detail="Audio not found")
    # Check format
    if not audio_path.endswith('.wav'):
        raise HTTPException(status_code=400,
                            detail="Audio must be .wav")
    # Load audio
    try:
        sample, _ = torchaudio.load(audio_path)
    except:
        raise HTTPException(status_code=400,
                            detail="Failed loading audio")

    gender = model(sample)

    return gender


parser = argparse.ArgumentParser(description='Model service')
parser.add_argument('--port', '-p', type=int, default=9050)
args = parser.parse_args()

# Run server
uvicorn.run(app, host='0.0.0.0', port=args.port)
